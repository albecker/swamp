=====
swamp
=====


.. image:: https://img.shields.io/pypi/v/swamp.svg
        :target: https://pypi.python.org/pypi/swamp

.. image:: https://img.shields.io/travis/phylex/swamp.svg
        :target: https://travis-ci.com/phylex/swamp

.. image:: https://readthedocs.org/projects/swamp/badge/?version=latest
        :target: https://swamp.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




The SoftWare Architectural Mirroring Platform for the HGCAL Detector


* Free software: MIT license
* Documentation: https://swamp.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
